package com.luay.interview.controller;

import com.luay.interview.model.LoginModel;
import com.luay.interview.model.LoginResponseModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;



@RestController
public class ApiController {

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @NonNull LoginModel model){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();

        map.add("client_id", model.getClientId());

        map.add("username", model.getUserName());

        map.add("password", model.getPassword());

        map.add("grant_type", model.getGrantType());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity< MultiValueMap<String, String>>(map, headers);

        RestTemplate restTemplate = new RestTemplate();

        String keyCloakUrl = "http://localhost:8180/realms/SpringBootKeycloak/protocol/openid-connect/token";

        ResponseEntity<LoginResponseModel> response =  restTemplate.postForEntity(keyCloakUrl, request, LoginResponseModel.class);

        return new ResponseEntity<>(response.getBody(), response.getStatusCode());

    }


    @GetMapping("/user")
    public String getUserPage(){
        return "you are a user";
    }

    @GetMapping("/admin")
    public String getAdminPage(){
        return "you are an admin";
    }
}
